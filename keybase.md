### Keybase proof

I hereby claim:

  * I am akavel on github.
  * I am akavel (https://keybase.io/akavel) on keybase.
  * I have a public key ASCLo2zmWoWzBw53_7qKB1FJx4HQIXd9cOi-vzmLu0TNTQo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "01208ba36ce65a85b3070e77ffba8a075149c781d021777d70e8bebf398bbb44cd4d0a",
      "host": "keybase.io",
      "kid": "01208ba36ce65a85b3070e77ffba8a075149c781d021777d70e8bebf398bbb44cd4d0a",
      "uid": "f576747758ef0706e33c725320592d19",
      "username": "akavel"
    },
    "merkle_root": {
      "ctime": 1506547343,
      "hash": "9c8a0096a702571bd249ad7bf1b22add831c86a34ad5bb0e5e8c25a35d8fcda24bd724f45b036517a7f4bce33dd196a80cbe0a02416df28e9820a87de250ff15",
      "hash_meta": "3afe7dfafce8052dabc57c561a91b2960394b93f53775bada15799c4a5ae3444",
      "seqno": 1467188
    },
    "service": {
      "name": "github",
      "username": "akavel"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.30"
  },
  "ctime": 1506547367,
  "expire_in": 504576000,
  "prev": "f46f17f5dd103cf519ec2470012d6f4301e7b338496f8ddda5490882d411d872",
  "seqno": 7,
  "tag": "signature"
}
```

with the key [ASCLo2zmWoWzBw53_7qKB1FJx4HQIXd9cOi-vzmLu0TNTQo](https://keybase.io/akavel), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgi6Ns5lqFswcOd/+6igdRSceB0CF3fXDovr85i7tEzU0Kp3BheWxvYWTFAzh7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwOGJhMzZjZTY1YTg1YjMwNzBlNzdmZmJhOGEwNzUxNDljNzgxZDAyMTc3N2Q3MGU4YmViZjM5OGJiYjQ0Y2Q0ZDBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwOGJhMzZjZTY1YTg1YjMwNzBlNzdmZmJhOGEwNzUxNDljNzgxZDAyMTc3N2Q3MGU4YmViZjM5OGJiYjQ0Y2Q0ZDBhIiwidWlkIjoiZjU3Njc0Nzc1OGVmMDcwNmUzM2M3MjUzMjA1OTJkMTkiLCJ1c2VybmFtZSI6ImFrYXZlbCJ9LCJtZXJrbGVfcm9vdCI6eyJjdGltZSI6MTUwNjU0NzM0MywiaGFzaCI6IjljOGEwMDk2YTcwMjU3MWJkMjQ5YWQ3YmYxYjIyYWRkODMxYzg2YTM0YWQ1YmIwZTVlOGMyNWEzNWQ4ZmNkYTI0YmQ3MjRmNDViMDM2NTE3YTdmNGJjZTMzZGQxOTZhODBjYmUwYTAyNDE2ZGYyOGU5ODIwYTg3ZGUyNTBmZjE1IiwiaGFzaF9tZXRhIjoiM2FmZTdkZmFmY2U4MDUyZGFiYzU3YzU2MWE5MWIyOTYwMzk0YjkzZjUzNzc1YmFkYTE1Nzk5YzRhNWFlMzQ0NCIsInNlcW5vIjoxNDY3MTg4fSwic2VydmljZSI6eyJuYW1lIjoiZ2l0aHViIiwidXNlcm5hbWUiOiJha2F2ZWwifSwidHlwZSI6IndlYl9zZXJ2aWNlX2JpbmRpbmciLCJ2ZXJzaW9uIjoxfSwiY2xpZW50Ijp7Im5hbWUiOiJrZXliYXNlLmlvIGdvIGNsaWVudCIsInZlcnNpb24iOiIxLjAuMzAifSwiY3RpbWUiOjE1MDY1NDczNjcsImV4cGlyZV9pbiI6NTA0NTc2MDAwLCJwcmV2IjoiZjQ2ZjE3ZjVkZDEwM2NmNTE5ZWMyNDcwMDEyZDZmNDMwMWU3YjMzODQ5NmY4ZGRkYTU0OTA4ODJkNDExZDg3MiIsInNlcW5vIjo3LCJ0YWciOiJzaWduYXR1cmUifaNzaWfEQLiL0MYcTKfk4M0HFDoWqNK+zNOjAeFuMlOQBAtYPprRB+aWTE8w9woGUZG3BX0TWqYFW6XilKyhs30jSQtUMQuoc2lnX3R5cGUgpGhhc2iCpHR5cGUIpXZhbHVlxCBPV67bO6FJFvkgWJqYZcqSZCVxGduJPRv7wOnmoPtCQqN0YWfNAgKndmVyc2lvbgE=

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/akavel

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id akavel
```